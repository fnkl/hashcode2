from pizza import Pizza
from finkel import parse_matrix
import numpy as np

def get_max_column_strip(pizza, i, j):
    assert isinstance(pizza,Pizza)
    tomatos=0
    mushrooms=0
    for row in range(pizza.h+1):
        if i+row >= pizza.getR():
            break
        if pizza.data[i + row, j] == pizza.TOMATO:
            tomatos += 1
        elif pizza.data[i + row, j] == pizza.MUSHROOM:
            mushrooms += 1
        else:
            break
    if tomatos >= pizza.l and mushrooms >= pizza.l:
        pizza.data[i: row, j: j + 1] += pizza.SLICED
        add_slice(pizza,i,j,row-1,j)

def find_solution(pizza):
    assert isinstance(pizza, Pizza)
    for row in range(pizza.getR()):
        for col in range(pizza.getC()):
            get_max_column_strip(pizza,row,col)

def improved_find_solution_version_one(pizza):
    _find_vertical_solution(pizza)
    _find_horizontal_solution(pizza)

def _find_vertical_solution(pizza):
    assert isinstance(pizza, Pizza)
    for row in range(pizza.getR()):
        for col in range(pizza.getC()):
            get_max_column_strip(pizza, row, col)

def _find_horizontal_solution(pizza):
    pizza.rotate()
    _find_vertical_solution(pizza)
    pizza.rotate()
    pizza.rotate()
    pizza.rotate()

def add_slice(pizza,r1,c1,r2,c2):
    if not pizza.rotated:
        pizza.slices.append([r1,c1,r2,c2])
    else:
        pizza.slices.append([c1,pizza.getC() - r2,c2, pizza.getC() - r1])

def score(pizza):
    return pizza.score()
