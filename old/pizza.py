import numpy as np

class Pizza:
    TOMATO=0
    MUSHROOM=1
    SLICED=2
    SLICED_MUSHROOM = 2
    SLICED_TOMATO   = 3

    def __init__(self,r,c,l,h):
        self.r = r
        self.c = c
        self.l = l
        self.h = h
        self.data = np.zeros((r,c,))
        self.slices=[]
        self.rotated=False

    def rotate(self):
        self.data = np.rot90(self.data)
        self.rotated = not self.rotated


    def getR(self):
        if self.rotated:
            return self.c
        return self.r

    def getC(self):
        if self.rotated:
            return self.r
        return self.c

    def score(self):
        return np.count_nonzero(self.data == 2)

    def score_slices(self):
        return sum((r2 - r1 + 1) * (c2 - c1 + 1) for (r1, c1, r2, c2) in self.slices)

    def print_slices(self):
        return '\n'.join([str(s) for s in self.slices])
