#!/usr/bin/env python

from finkel import *
from iddo   import *
from itamar import *

import sys

def main(path):
    p = parse_matrix(path)
    find_solution(p)
    print(score(p))
    improve_by_rect(p)
    print(score(p))
    write_sol(p.slices)


if __name__ == "__main__":
    main(sys.argv[1])
