from pizza import Pizza
import itertools as it
import numpy as np


def parse_matrix(path):
    with open(path, 'r') as f:
        [r, c, l, h] = map(int, next(f).split())
        res = Pizza(r, c, l, h)
        ln = 0
        for line in f:
            res.data[ln] = parse_line(line)
            ln += 1
    return res


def parse_line(line):
    return [Pizza.TOMATO if x == 'T' else Pizza.MUSHROOM for x in line.strip()]



def write_sol(slices,filename = "pizza_result.txt"):
    with open(filename, 'w+') as f:
        f.write("{}\n".format(len(slices)))
        for s in slices:
            f.write(str(s).strip('[]()').replace(',','') + "\n")


def improve_by_rect(p):
    improved = True
    while improved:
        slice_list = []
        improved = False
        for s in p.slices:
            best_slice = improve_slice(p, s)
            slice_list += [best_slice]
            if best_slice != s:
                improved = True
        p.slices = slice_list


def improve_slice(p, s):
    [r1, c1, r2, c2] = s
    m = p.data
    sm = m[r1:r2+1, c1:c2+1]
    sm -= (Pizza.SLICED) 
    best_score = sm.size
    best_slice = []
    best_matrix = None

    for (i, j) in intervals(range(r1, r2+1)):
        if c1 > 0:
            candidate = m[i:j+1, c1-1:c2+1]
            if is_valid(candidate, p) and candidate.size > best_score:
                best_score = candidate.size
                best_slice = [i, c1-1, j, c2]
                best_matrix = candidate

        if c2 < p.c:
            candidate = m[i:j+1, c1:c2+2]
            if is_valid(candidate, p) and candidate.size > best_score:
                best_score = candidate.size
                best_slice = [i, c1, j, c2+1]
                best_matrix = candidate

    if best_slice:
        best_matrix += Pizza.SLICED
        return best_slice
    sm += Pizza.SLICED
    return s

def intervals(t):
    return ((x,y) for (x,y) in it.product(t, t) if x < y)


def is_valid(m, p):
    return m.size <= p.h \
       and np.count_nonzero(m == Pizza.TOMATO)   >= p.l \
       and np.count_nonzero(m == Pizza.MUSHROOM) >= p.l \
       and 2 not in m

