from glob import glob
import csv
from finkel import parse_matrix
from iddo import score, find_solution

class TestExamples(object):
    def __init__(self):
        self.scores = {'small': 0, 'medium': 0, 'big': 0, 'example': 0}
        # try:
        with open('tests/scores.csv', 'rU') as f:
            reader = csv.reader(f)
            for row in reader:
                if row:
                    self.scores[row[0]] = int(row[1])
        self.files = glob('tests/*in')

    def write_results(self):
        with open('tests/scores.csv', 'wb') as f:
            writer = csv.writer(f)
            for name_score in self.scores.iteritems():
                writer.writerow(name_score)

    def test_examples(self):
        for name in ['example', 'small', 'medium', 'big']:
            try:
                self.t(name)
                print "{} is good: {}".format(name, self.scores[name])
            except AssertionError as ex:
                print ex

    def t(self, name):
        p = parse_matrix('tests/{}.in'.format(name))
        find_solution(p)
        p_score = p.score()
        assert p_score >= self.scores[name], 'FAIL! {} was: {} now: {}'.format(name, self.scores[name], p_score)

if __name__ == '__main__':
    t = TestExamples()
    t.test_examples()
    t.write_results()
